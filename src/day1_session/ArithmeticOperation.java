package day1_session;

public class ArithmeticOperation {

	public static void main(String[] args) {
		int a=5, b=6;
		add(a,b);
		int data=sub(a,b);
		System.out.println(data);
	}
	public static void add(int a, int b) {
		int result=a+b;
		System.out.println(result);
	}
	public static int sub(int a, int b) {
		int result=a-b;
		return result;

	}

}
