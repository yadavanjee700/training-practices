package day1_session;

import java.util.Scanner;

public class SwapNumber {

	public static void main(String[] args) {
		int a,b;
		Scanner sc = new Scanner(System.in);
		System.out.println("please enter the values of a and b");
		a=sc.nextInt();
		b=sc.nextInt();
		System.out.println("before swapping");
		System.out.println("value of a="+a+" value of b="+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("after swapping");
		System.out.println("value of a="+a+" value of b="+b);
			

	}

}
