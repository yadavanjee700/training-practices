package day2_session;


import java.util.Arrays;

public class ArrayDemo {

	public static void main(String[] args) {
	int[] empId=new int[4];
	empId[0]=1;
	empId[2]=3;
	empId[3]=6;
	
	System.out.println(Arrays.toString(empId));
	System.out.println("Value stored at 2nd index is"+empId[2]);
	for(int i=0;i<empId.length;i++) {
		System.out.println("value stored at "+i+"th index is "+empId[i]);
	}
	System.out.println("output using enhanced for loop");
	for(int emp:empId) {
		System.out.println(emp);
	}
	

	}

}
