package day2_session;

import java.util.Scanner;

public class CheckPalindrome {

	public static void main(String[] args) {
		int number,reverseNumber=0,result;
		System.out.println("enter the number");
		Scanner sc= new Scanner(System.in);
		number=sc.nextInt();
		int initialValue=number;
		while(number!=0) {
			result=number%10;
			reverseNumber=reverseNumber*10+result;
			number=number/10;
		}
		System.out.println(reverseNumber);
		if(initialValue==reverseNumber) {
			System.out.println("its an palindrome");
		}else
			System.out.println("its not an palindrome");
		sc.close();
				
					
		}

	}


