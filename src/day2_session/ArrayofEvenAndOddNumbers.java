package day2_session;


public class ArrayofEvenAndOddNumbers {

	public static void main(String[] args) {

		int a[] = { 12, 23, 45, 32, 67};
		System.out.println("Odd Numbers:" );
		for (int i = 0; i < a.length; i++) {
			if (a[i] % 2 != 0) {
				System.out.println(a[i]);
			}
		}
		System.out.println("Even Numbers:");
		for (int i = 0; i < a.length; i++) {
			if (a[i] % 2 == 0) {
				System.out.println(a[i]);
			}
		}
	}

}
