package day2_session;

class ToPrintFibonacciSeries {
	
	public static void main(String args[]) {
		
		int a = 0, b = 1, c, i, count = 10;
		System.out.print(a + " " + b);

		for (i = 2; i < count; ++i)
		{
			c = a + b;//a=1,b=2,c=3
			
			System.out.print(" " + c);//c=3
			
			a = b;//a=2
			b = c;//b=3
		

	}
}
}