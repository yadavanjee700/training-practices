package day3_Inheritance;

public class RBI {

	public int totalNoOfTransaction=5;
	public String customer1="Anjee";
	public String customer2="Rajesh";
	
	public void customerService() {
		System.out.println("customer details are \n1."+customer1+"\n2."+customer2);
	}
	public void transactionLimit() {
		System.out.println("total number of transaction allowed in a month are "+totalNoOfTransaction);
	}
}
