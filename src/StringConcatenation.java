import java.util.Scanner;

public class StringConcatenation {

	public static void main(String[] args) {
		String firstName,lastName,fullName;
		System.out.println("enter the name");
		Scanner sc=new Scanner(System.in);
		firstName=sc.next(); lastName=sc.next();
		System.out.println("entered name is "+firstName);
		System.out.println("entered last name is "+lastName);
		fullName=firstName.concat(" ").concat(lastName);
		System.out.println("full name is "+fullName);
		
		
		System.out.println(fullName.toUpperCase());
		System.out.println(fullName.toLowerCase());
		System.out.println("length of a string is "+fullName.length());

	}

}
