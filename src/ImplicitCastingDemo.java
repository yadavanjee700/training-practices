
public class ImplicitCastingDemo {

	public static void main(String[] args) {
		short x=1;
		int y=x+1;
		System.out.println(y);
		float f=12.0f;
		int z= (int) (f+2);
		System.out.println(z);

	}

}
