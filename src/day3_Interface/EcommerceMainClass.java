package day3_Interface;

public class EcommerceMainClass {
	
	public static void main(String[]args) {
		FlipKart flipkart = new FlipKart();
		String member=flipkart.memberDetails("Anjee");
		System.out.println("member logged in is "+member);
		
		Ecommerce ecom= new FlipKart();
		ecom.memberDetails("Anjee");
		
		ecom= new Amazon();
		int Order=ecom.purchaseOrder(5);
		System.out.println("total orders placed are "+order);
	}

}
