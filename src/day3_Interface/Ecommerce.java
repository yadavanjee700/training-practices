package day3_Interface;

public interface Ecommerce {
	public abstract String memberDetails(String memberName);
	
	public abstract int purchaseOrder(int noOfOrders);

	String transactionDetails();

}
