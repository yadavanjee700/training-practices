package day2_classdemo;

import java.util.Scanner;

public class MainBanking {

	public static void main(String[] args) {
		ATMBanking atmBanking=new ATMBanking();
		int currentBalance=atmBanking.balanceEnquiry();
		System.out.println("current balance in account is \n"+currentBalance);
		System.out.println("please enter the amount to withdraw");
		Scanner sc=new Scanner(System.in);
		int amount=sc.nextInt();
		currentBalance=atmBanking.cashwithdraw(currentBalance, amount);
		System.out.println("amount after withdrawal is \n"+currentBalance);
		atmBanking.miniStatement();

	}

}
