package day2_abstractionDemo;

public abstract class BankingService {
	public abstract void smsService();
	
	public abstract int balanceEnquiry();
	
	public abstract int cashwithdrawal(int cashwithdrawal );

}
